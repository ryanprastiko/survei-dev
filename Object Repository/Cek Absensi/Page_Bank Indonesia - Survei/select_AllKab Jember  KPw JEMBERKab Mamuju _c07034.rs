<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_AllKab Jember  KPw JEMBERKab Mamuju _c07034</name>
   <tag></tag>
   <elementGuidId>7e5c45b9-c23d-4beb-b90c-7561ad608161</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='MainContent_cmbKota']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$MainContent$cmbKota</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>MainContent_cmbKota</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
	All
	Kab. Jember / KPw JEMBER
	Kab. Mamuju / KPw PROVINSI SULAWESI BARAT
	Kab. Manokwari / KPw PROVINSI PAPUA BARAT
	Kab. Purwokerto / KPw PURWOKERTO
	Kota Ambon / KPw PROVINSI MALUKU
	Kota Banda Aceh / KPw PROVINSI ACEH
	Kota Bandar Lampung / KPw PROVINSI LAMPUNG
	Kota Bandung / KPw PROVINSI JAWA BARAT
	Kota Banjarmasin / KPw PROVINSI KALIMANTAN SELATAN
	Kota Batam / KPw PROVINSI KEPULAUAN RIAU
	Kota Bengkulu / KPw PROVINSI BENGKULU
	Kota Cirebon / KPw CIREBON
	Kota Denpasar / KPw PROVINSI BALI
	Kota Gorontalo / KPw PROVINSI GORONTALO
	Kota Jakarta Pusat / KPw PROVINSI DKI JAKARTA
	Kota Jambi / KPw PROVINSI JAMBI
	Kota Jayapura / KPw PROVINSI PAPUA
	Kota Kediri / KPw KEDIRI
	Kota Kendari / KPw PROVINSI SULAWESI TENGGARA
	Kota Kupang / KPw PROVINSI NUSA TENGGARA TIMUR
	Kota Lhokseumawe / KPw LHOKSEUMAWE
	Kota Makassar / KPw PROVINSI SULAWESI SELATAN
	Kota Malang / KPw MALANG
	Kota Manado / KPw PROVINSI SULAWESI UTARA
	Kota Mataram / KPw PROVINSI NUSA TENGGARA BARAT
	Kota Medan / KPw PROVINSI SUMATERA UTARA
	Kota Padang / KPw PROVINSI SUMATERA BARAT
	Kota Palangkaraya / KPw PROVINSI KALIMANTAN TENGAH
	Kota Palembang / KPw PROVINSI SUMATERA SELATAN
	Kota Palu / KPw PROVINSI SULAWESI TENGAH
	Kota Pangkal Pinang / KPw PROVINSI KEPULAUAN BANGKA BELITUNG
	Kota Pekanbaru / KPw PROVINSI RIAU
	Kota Pontianak / KPw PROVINSI KALIMANTAN BARAT
	Kota Samarinda / KPw PROVINSI KALIMANTAN TIMUR
	Kota Semarang / KPw PROVINSI JAWA TENGAH
	Kota Serang / KPw PROVINSI BANTEN
	Kota Surabaya / KPw PROVINSI JAWA TIMUR
	Kota Surakarta (Solo) / KPw SOLO
	Kota Tarakan / KPw PROVINSI KALIMANTAN UTARA
	Kota Tasikmalaya / KPw TASIKMALAYA
	Kota Tegal / KPw TEGAL
	Kota Ternate / KPw PROVINSI MALUKU UTARA
	Kota Yogyakarta / KPw PROVINSI D.I YOGYAKARTA

</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;MainContent_cmbKota&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='MainContent_cmbKota']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='div1']/div/table/tbody/tr[2]/td[3]/select</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=':'])[4]/following::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kota'])[1]/following::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kota/KPw'])[1]/preceding::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Target Responden'])[1]/preceding::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[2]/td[3]/select</value>
   </webElementXpaths>
</WebElementEntity>
