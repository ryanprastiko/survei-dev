<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select__SemuaAll_Bengkulu (KPw PROVINSI BEN_9a22ae</name>
   <tag></tag>
   <elementGuidId>febd8349-d9c7-4978-9d41-ef34a657cfb8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='cmbKantor']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>cmbKantor</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>javascript:setTimeout('__doPostBack(\'cmbKantor\',\'\')', 0)</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>cmbKantor</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
	_Semua/All_
	Bengkulu (KPw PROVINSI BENGKULU)
	Jakarta Pusat (KANTOR PUSAT JAKARTA)
	Jakarta Pusat (KPw PROVINSI DKI JAKARTA)
	Kab. Jember (KPw JEMBER)
	Kab. Mamuju (KPw PROVINSI SULAWESI BARAT)
	Kab. Manokwari (KPw PROVINSI PAPUA BARAT)
	Kab. Purwokerto (KPw PURWOKERTO)
	Kota Ambon  (KPw PROVINSI MALUKU)
	Kota Balikpapan  (KPw BALIKPAPAN)
	Kota Banda Aceh (KPw PROVINSI ACEH)
	Kota Bandar Lampung  (KPw PROVINSI LAMPUNG)
	Kota Bandung (KPw PROVINSI JAWA BARAT)
	Kota Banjarmasin (KPw PROVINSI KALIMANTAN SELATAN)
	Kota Batam (KPw PROVINSI KEPULAUAN RIAU)
	Kota Cirebon (KPw CIREBON)
	Kota Denpasar (KPw PROVINSI BALI)
	Kota Gorontalo (KPw PROVINSI GORONTALO)
	Kota Jambi (KPw PROVINSI JAMBI)
	Kota Jayapura (KPw PROVINSI PAPUA)
	Kota Kediri (KPw KEDIRI)
	Kota Kendari (KPw PROVINSI SULAWESI TENGGARA)
	Kota Kupang (KPw PROVINSI NUSA TENGGARA TIMUR)
	Kota Lhokseumawe (KPw LHOKSEUMAWE)
	Kota Makasar (KPw PROVINSI SULAWESI SELATAN)
	Kota Malang (KPw MALANG)
	Kota Mataram (KPw PROVINSI NUSA TENGGARA BARAT)
	Kota Medan (KPw PROVINSI SUMATERA UTARA)
	Kota Padang (KPw PROVINSI SUMATERA BARAT)
	Kota Palangkaraya (KPw PROVINSI KALIMANTAN TENGAH)
	Kota Palembang (KPw PROVINSI SUMATERA SELATAN)
	Kota Palu (KPw PROVINSI SULAWESI TENGAH)
	Kota Pangkal Pinang (KPw PROVINSI KEPULAUAN BANGKA BELITUNG)
	Kota Pekanbaru (KPw PROVINSI RIAU)
	Kota Pematang Siantar (KPw PEMATANG SIANTAR)
	Kota Pontianak (KPw PROVINSI KALIMANTAN BARAT)
	Kota Samarinda (KPw PROVINSI KALIMANTAN TIMUR)
	Kota Semarang (KPw PROVINSI JAWA TENGAH)
	Kota Serang (KPw PROVINSI BANTEN)
	Kota Sibolga  (KPw SIBOLGA)
	Kota Surabaya (KPw PROVINSI JAWA TIMUR)
	Kota Surakarta (KPw SOLO)
	Kota Tarakan (KPw PROVINSI KALIMANTAN UTARA)
	Kota Tasikmalaya (KPw TASIKMALAYA)
	Kota Tegal (KPw TEGAL)
	Kota Ternate (KPw PROVINSI MALUKU UTARA)
	Kota Yogyakarta  (KPw PROVINSI D.I YOGYAKARTA)
	Manado (KPw PROVINSI SULAWESI UTARA)

</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;cmbKantor&quot;)</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Tambah User SPIME/Page_Bank Indonesia - Survei/iframe</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='cmbKantor']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form1']/div[3]/table/tbody/tr[3]/td[3]/select</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=':'])[3]/following::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='KP/KPw'])[1]/following::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Survei'])[1]/preceding::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Level Hak Akses'])[1]/preceding::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select</value>
   </webElementXpaths>
</WebElementEntity>
