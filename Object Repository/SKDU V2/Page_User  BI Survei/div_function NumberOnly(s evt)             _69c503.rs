<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_function NumberOnly(s evt)             _69c503</name>
   <tag></tag>
   <elementGuidId>4505224a-7d85-46ea-b5e7-a0b053c37f1f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='form2']/div[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            

    
        function NumberOnly(s, evt) {
            var charCode = (evt.htmlEvent.which) ? evt.htmlEvent.which : event.keyCode
            if (charCode > 31 &amp;&amp; (charCode &lt; 48 || charCode > 57))
                _aspxPreventEvent(evt.htmlEvent);
        }

        function CekAngka(s, evt) {
            var charCode = (evt.htmlEvent.which) ? evt.htmlEvent.which : event.keyCode
            if (charCode > 31 &amp;&amp; (charCode &lt; 48 || charCode > 57))
                _aspxPreventEvent(evt.htmlEvent);
        }

        function CekHuruf(s, evt) {
            var charCode = (evt.htmlEvent.which) ? evt.htmlEvent.which : event.keyCode
            if (charCode > 31 &amp;&amp; (charCode &lt; 48 || charCode > 57) &amp;&amp; (charCode &lt; 65 || charCode > 90) &amp;&amp; (charCode &lt; 97 || charCode > 122) &amp;&amp; (charCode &lt; 45 || charCode > 46) &amp;&amp; (charCode &lt; 95 || charCode > 95))
                _aspxPreventEvent(evt.htmlEvent);
        }

        var postponedCallbackValueRsp = null;
        function OnEndCallbackRsp(s, e) {
            CL_user_secret_key.SetText(CL_hidField.Get('passValue'));

            if (postponedCallbackValueRsp != null) {
                CL_pnlName.PerformCallback(postponedCallbackValueRsp);
                postponedCallbackValueRsp = null;

            }
        }

        function LdapSelected() {
            //CL_pnlName.PerformCallback('LdapSelected___');
            CL_Callback1.PerformCallback('ldapselected');
        }
        function LdapCleared() {
            CL_pnlName.PerformCallback('LdapCleared___');
        }
        function ShowCbo() {
            CL_pnlName.PerformCallback('ShowCbo___');
        }
        function OnCallBackComplete(s, e) {
            var outputs = e.result.toString();
            var output = outputs.split(&quot;___&quot;);

            if (output[0] == 'ldapselected') {
                CL_edit_user_nama.SetText(output[1]);
                CL_edit_user_email.SetText(output[2]);
            } else if (output[0] == 'ldapcleared') {
                CL_edit_user_nama.SetText('');
                CL_edit_user_email.SetText('');
            }
        }
        function ConfirmFirst(s, e) {
            var ask = confirm('Konfirmasi untuk mereset password menjadi default : 123456a!');
            if (ask) {
                LoadingPanel.Show();
            } else {
                e.processOnServer = false;
            }
        }
    
    
&lt;!--

var dxo = new ASPxClientCallback('cpContent_Callback1');
window['CL_Callback1'] = dxo;
dxo.callBack = function(arg) { WebForm_DoCallback('ctl00$cpContent$Callback1',arg,aspxCallback,'cpContent_Callback1',aspxCallbackError,true); };
dxo.uniqueID = 'ctl00$cpContent$Callback1';
dxo.CallbackComplete.AddHandler(OnCallBackComplete);
dxo.AfterCreate();

//-->

    
	
                
&lt;!--

var dxo = new ASPxClientHiddenField('cpContent_pnlName_hidField');
window['CL_hidField'] = dxo;
dxo.callBack = function(arg) { WebForm_DoCallback('ctl00$cpContent$pnlName$hidField',arg,aspxCallback,'cpContent_pnlName_hidField',aspxCallbackError,true); };
dxo.uniqueID = 'ctl00$cpContent$pnlName$hidField';
dxo.properties = {'dxppassValue':''};
dxo.AfterCreate();

//-->

                
                    
                        Jenis User *
                        
                        :
                        
                        
                            
		
			
				
					
						
							Non BI
						
					
&lt;!--
aspxAddDisabledItems('cpContent_pnlName_rbLdap_RB0',[[[''],[''],['']]]);

//-->

						
							BI
						
					
&lt;!--
aspxAddDisabledItems('cpContent_pnlName_rbLdap_RB1',[[[''],[''],['']]]);

//-->

				
			
		
	
&lt;!--
aspxAddDisabledItems('cpContent_pnlName_rbLdap',[[['dxeDisabled_DevEx'],[''],['','RB0','RB1']]]);

var dxo = new ASPxClientRadioButtonList('cpContent_pnlName_rbLdap');
window['cpContent_pnlName_rbLdap'] = dxo;
dxo.uniqueID = 'ctl00$cpContent$pnlName$rbLdap';
dxo.ValueChanged.AddHandler(function(s,e){ CL_pnlName.PerformCallback('check___'); } );
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle('F','dxeFocused_DevEx','');
dxo.savedSelectedIndex = 0;
dxo.CreateItems([['Non BI','NON LDAP',''],['BI','LDAP','']]);
dxo.imageProperties = {'4':['dxEditors_edtRadioButtonChecked_DevEx','dxEditors_edtRadioButtonUnchecked_DevEx'],'8':['dxEditors_edtRadioButtonCheckedDisabled_DevEx','dxEditors_edtRadioButtonUncheckedDisabled_DevEx']};
dxo.icbFocusedStyle = ['dxeIRBFocused_DevEx',''];
dxo.AfterCreate();

//-->

                        
                    
                    
                        Id User *
                        
                        :
                        
                        
                            
		
                                        
			
				
					
						
					
				
					
						Invalid value
					
				
			
		
&lt;!--
aspxAddDisabledItems('cpContent_pnlName_nonLdap_edit_user_id',[[['dxeDisabled_DevEx'],[''],['','I']]]);
document.getElementById(&quot;cpContent_pnlName_nonLdap_edit_user_id_ET&quot;).setAttribute(&quot;errorFrame&quot;, &quot;errorFrame&quot;);

var dxo = new ASPxClientTextBox('cpContent_pnlName_nonLdap_edit_user_id');
window['CL_edit_user_id'] = dxo;
dxo.uniqueID = 'ctl00$cpContent$pnlName$nonLdap$edit_user_id';
dxo.KeyPress.AddHandler(function(s,e){CekHuruf(s,e);});
dxo.validationGroup = &quot;validate&quot;;
dxo.customValidationEnabled = true;
dxo.isValid = true;
dxo.errorText = 'Invalid value';
dxo.validationPatterns = [ new ASPxRequiredFieldValidationPattern('Harus diisi') ];
dxo.display = &quot;Dynamic&quot;;
dxo.errorImageIsAssigned = true;
dxo.controlCellStyles = { cssClass: 'dxeErrorFrame_DevEx dxeErrorFrameSys dxeNoBorderRight dxeControlsCell_DevEx', style: '' };
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle('I','dxeInvalid_DevEx','');
dxo.styleDecoration.AddStyle('F','dxeFocused_DevEx','');
dxo.AfterCreate();

//-->

                                    
	
                            
		
                                        
			
				
					
						
					
						Cari LDAP
					
				
					
						
            
                
                    Search:
                    
                    
                        
							
								
							
						
&lt;!--
aspxAddDisabledItems('cpContent_pnlName_ldap_userLdap_popupWindow_txtCari',[[['dxeDisabled_DevEx'],[''],['','I']]]);

var dxo = new ASPxClientTextBox('cpContent_pnlName_ldap_userLdap_popupWindow_txtCari');
window['cpContent_pnlName_ldap_userLdap_popupWindow_txtCari'] = dxo;
dxo.uniqueID = 'ctl00$cpContent$pnlName$ldap$userLdap$popupWindow$txtCari';
dxo.KeyDown.AddHandler(function(s,e) {if(e.htmlEvent.keyCode == 13){CL_cpContent_pnlName_ldap_userLdap_popupWindow_btnCari.DoClick();}});
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle('F','dxeFocused_DevEx','');
dxo.AfterCreate();

//-->

                    
                    
                        
							
								Cari
							
						
&lt;!--
aspxAddHoverItems('cpContent_pnlName_ldap_userLdap_popupWindow_btnCari',[[['dxbButtonHover_DevEx'],[''],[''],['','TC']]]);
aspxAddPressedItems('cpContent_pnlName_ldap_userLdap_popupWindow_btnCari',[[['dxbButtonPressed_DevEx'],[''],[''],['','TC']]]);
aspxAddDisabledItems('cpContent_pnlName_ldap_userLdap_popupWindow_btnCari',[[['dxbDisabled_DevEx'],[''],[''],['','TC']]]);

var dxo = new ASPxClientButton('cpContent_pnlName_ldap_userLdap_popupWindow_btnCari');
window['CL_cpContent_pnlName_ldap_userLdap_popupWindow_btnCari'] = dxo;
dxo.uniqueID = 'ctl00$cpContent$pnlName$ldap$userLdap$popupWindow$btnCari';
dxo.Click.AddHandler(function(s,e){ CL_cpContent_pnlName_ldap_userLdap_popupWindow_ASPxGridView1.Refresh(); });
aspxAddSelectedItems('cpContent_pnlName_ldap_userLdap_popupWindow_btnCari',[[['dxbf'],[''],['CD']]]);
dxo.causesValidation = false;
dxo.AfterCreate();

//-->

                    
                
            
            
            
            
							
								
									
										
											
												Kode 
											
										
											
												Nama 
											
										
											
												Email 
											
										
									
										
											No data to display
										
									
								
									
										
											Loading…
										
									
								

								
									
										
									
										
									
										
									
										
									
										
									
										
									
								
							
						
&lt;!--

var dxo = new ASPxClientGridView('cpContent_pnlName_ldap_userLdap_popupWindow_ASPxGridView1');
window['CL_cpContent_pnlName_ldap_userLdap_popupWindow_ASPxGridView1'] = dxo;
dxo.callBack = function(arg) { WebForm_DoCallback('ctl00$cpContent$pnlName$ldap$userLdap$popupWindow$ASPxGridView1',arg,aspxCallback,'cpContent_pnlName_ldap_userLdap_popupWindow_ASPxGridView1',aspxCallbackError,true); };
dxo.uniqueID = 'ctl00$cpContent$pnlName$ldap$userLdap$popupWindow$ASPxGridView1';
dxo.RowDblClick.AddHandler(function(s,e){ CL_cpContent_pnlName_ldap_userLdap_popupWindow_ASPxGridView1.GetSelectedFieldValues('myVal;UserID', function (selectedValues) {
        for (i = 0; i &lt; selectedValues.length; i++) {
            var key = selectedValues[i][0];
            var text = selectedValues[i][1];
            var hidTextField = document.getElementById('cpContent_pnlName_ldap_userLdap_hidNama');
            hidTextField.value = text;
            var tempTextField = CL_cpContent_pnlName_ldap_userLdap_txtNama;
            tempTextField.SetValue(text);
            var hidValueField = document.getElementById('cpContent_pnlName_ldap_userLdap_hidValue');
            hidValueField.value = key;
            LdapSelected(key, text);
            return;
        }
    });   CL_cpContent_pnlName_ldap_userLdap_popupWindow.Hide(); });
dxo.callBacksEnabled=true;
dxo.pageRowCount=0;
dxo.pageRowSize=5;
dxo.pageIndex=0;
dxo.pageCount=0;
dxo.selectedWithoutPageRowCount=0;
dxo.filteredSelectedWithoutPageRowCount=0;
dxo.visibleStartIndex=0;
dxo.focusedRowIndex=-1;
dxo.allowFocusedRow=false;
dxo.allowSelectByRowClick=true;
dxo.allowSelectSingleRowOnly=true;
dxo.allowMultiColumnAutoFilter=false;
dxo.indentColumnCount=0;
dxo.callbackOnFocusedRowChanged=false;
dxo.callbackOnSelectionChanged=false;
dxo.autoFilterDelay='1200';
dxo.columns = [new ASPxClientGridViewColumn('',0,-1,'UserID',1,'',0,1,1,1,0,0,0),
new ASPxClientGridViewColumn('',1,-1,'UserName',1,'',0,1,1,1,0,0,0),
new ASPxClientGridViewColumn('',2,-1,'UserMail',1,'',0,1,1,1,0,0,0)];
dxo.editState=0;
dxo.editMode=2;
dxo.AfterCreate();

//-->

            
            
            
							
								Select
							
						
&lt;!--
aspxAddHoverItems('cpContent_pnlName_ldap_userLdap_popupWindow_btnSelect',[[['dxbButtonHover_DevEx'],[''],[''],['','TC']]]);
aspxAddPressedItems('cpContent_pnlName_ldap_userLdap_popupWindow_btnSelect',[[['dxbButtonPressed_DevEx'],[''],[''],['','TC']]]);
aspxAddDisabledItems('cpContent_pnlName_ldap_userLdap_popupWindow_btnSelect',[[['dxbDisabled_DevEx'],[''],[''],['','TC']]]);

var dxo = new ASPxClientButton('cpContent_pnlName_ldap_userLdap_popupWindow_btnSelect');
window['cpContent_pnlName_ldap_userLdap_popupWindow_btnSelect'] = dxo;
dxo.uniqueID = 'ctl00$cpContent$pnlName$ldap$userLdap$popupWindow$btnSelect';
dxo.Click.AddHandler(function(s,e){ CL_cpContent_pnlName_ldap_userLdap_popupWindow_ASPxGridView1.GetSelectedFieldValues('myVal;UserID', function (selectedValues) {
        for (i = 0; i &lt; selectedValues.length; i++) {
            var key = selectedValues[i][0];
            var text = selectedValues[i][1];
            var hidTextField = document.getElementById('cpContent_pnlName_ldap_userLdap_hidNama');
            hidTextField.value = text;
            var tempTextField = CL_cpContent_pnlName_ldap_userLdap_txtNama;
            tempTextField.SetValue(text);
            var hidValueField = document.getElementById('cpContent_pnlName_ldap_userLdap_hidValue');
            hidValueField.value = key;
            LdapSelected(key, text);
            return;
        }
    });   CL_cpContent_pnlName_ldap_userLdap_popupWindow.Hide(); });
aspxAddSelectedItems('cpContent_pnlName_ldap_userLdap_popupWindow_btnSelect',[[['dxbf'],[''],['CD']]]);
dxo.causesValidation = false;
dxo.AfterCreate();

//-->

        
					
				
			
		
&lt;!--

var dxo = new ASPxClientPopupControl('cpContent_pnlName_ldap_userLdap_popupWindow');
window['CL_cpContent_pnlName_ldap_userLdap_popupWindow'] = dxo;
dxo.callBack = function(arg) { WebForm_DoCallback('ctl00$cpContent$pnlName$ldap$userLdap$popupWindow',arg,aspxCallback,'cpContent_pnlName_ldap_userLdap_popupWindow',aspxCallbackError,true); };
dxo.uniqueID = 'ctl00$cpContent$pnlName$ldap$userLdap$popupWindow';
dxo.popupAnimationType='slide';
dxo.allowResize=true;
dxo.isPopupPositionCorrectionOn=false;
dxo.allowDragging=true;
dxo.width=200;
dxo.height=0;
dxo.defaultWindowPopupElementIDList=['cpContent_pnlName_ldap_userLdap_lblOpenWindow'];
dxo.autoUpdatePosition = true;
dxo.AfterCreate();

//-->


			
				
            
					
						
					
				
&lt;!--
aspxAddDisabledItems('cpContent_pnlName_ldap_userLdap_txtNama',[[['dxeDisabled_DevEx'],[''],['','I']]]);

var dxo = new ASPxClientTextBox('cpContent_pnlName_ldap_userLdap_txtNama');
window['CL_cpContent_pnlName_ldap_userLdap_txtNama'] = dxo;
dxo.uniqueID = 'ctl00$cpContent$pnlName$ldap$userLdap$txtNama';
dxo.readOnly=true;
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle('F','dxeFocused_DevEx','');
dxo.AfterCreate();

//-->

            
            
        
				
                
        
				
                
&lt;!--
aspxAddDisabledItems('cpContent_pnlName_ldap_userLdap_lblClearValue',[[['dxeDisabled_DevEx'],[''],['']]]);

var dxo = new ASPxClientLabel('cpContent_pnlName_ldap_userLdap_lblClearValue');
window['cpContent_pnlName_ldap_userLdap_lblClearValue'] = dxo;
dxo.uniqueID = 'ctl00$cpContent$pnlName$ldap$userLdap$lblClearValue';
dxo.Click.AddHandler(function(s,e){ var tempTextField = CL_cpContent_pnlName_ldap_userLdap_txtNama;
        tempTextField.SetValue('');
        var hidValueField = document.getElementById('cpContent_pnlName_ldap_userLdap_hidValue');
        hidValueField.value = '';LdapCleared(); });
dxo.AfterCreate();

//-->

        
			
		
		

                                    
	
&lt;!--

var dxo = new ASPxClientPanel('cpContent_pnlName_ldap');
window['cpContent_pnlName_ldap'] = dxo;
dxo.uniqueID = 'ctl00$cpContent$pnlName$ldap';
dxo.clientVisible = false;dxo.AfterCreate();

//-->

                            
                        
                    
                    
		Password User *
                        
                        
		:
                        
		
                            
			
				
					
						
					
				
					
						Invalid value
					
				
			
		
&lt;!--
aspxAddDisabledItems('cpContent_pnlName_user_secret_key',[[['dxeDisabled_DevEx'],[''],['','I']]]);
document.getElementById(&quot;cpContent_pnlName_user_secret_key_ET&quot;).setAttribute(&quot;errorFrame&quot;, &quot;errorFrame&quot;);

var dxo = new ASPxClientTextBox('cpContent_pnlName_user_secret_key');
window['CL_user_secret_key'] = dxo;
dxo.uniqueID = 'ctl00$cpContent$pnlName$user_secret_key';
dxo.validationGroup = &quot;validate&quot;;
dxo.customValidationEnabled = true;
dxo.isValid = true;
dxo.errorText = 'Invalid value';
dxo.validationPatterns = [ new ASPxRequiredFieldValidationPattern('Harus diisi'), new ASPxRegularExpressionValidationPattern('Password tidak valid', '^.*(?=.{8,})(?=.*\\d)(?=.*[a-zA-Z])(?=.*[\\!@#$%^&amp;\\*\\+_\\-=\\)\\(\\.\\,]).*$') ];
dxo.display = &quot;Dynamic&quot;;
dxo.errorImageIsAssigned = true;
dxo.controlCellStyles = { cssClass: 'dxeErrorFrame_DevEx dxeErrorFrameSys dxeNoBorderRight dxeControlsCell_DevEx', style: '' };
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle('I','dxeInvalid_DevEx','');
dxo.styleDecoration.AddStyle('F','dxeFocused_DevEx','');
dxo.AfterCreate();

//-->

                            Kata Sandi merupakan kombinasi huruf, angka dan simbol yang
                                terdiri dari minimal 8 karakter 
                        
	
	
                    
                        Nama User *
                        
                        :
                        
                        
                            
		
			
				
					
				
			
				
					Invalid value
				
			
		
	
&lt;!--
aspxAddDisabledItems('cpContent_pnlName_edit_user_nama',[[['dxeDisabled_DevEx'],[''],['','I']]]);
document.getElementById(&quot;cpContent_pnlName_edit_user_nama_ET&quot;).setAttribute(&quot;errorFrame&quot;, &quot;errorFrame&quot;);

var dxo = new ASPxClientTextBox('cpContent_pnlName_edit_user_nama');
window['CL_edit_user_nama'] = dxo;
dxo.uniqueID = 'ctl00$cpContent$pnlName$edit_user_nama';
dxo.validationGroup = &quot;validate&quot;;
dxo.customValidationEnabled = true;
dxo.isValid = true;
dxo.errorText = 'Invalid value';
dxo.validationPatterns = [ new ASPxRequiredFieldValidationPattern('Harus diisi') ];
dxo.display = &quot;Dynamic&quot;;
dxo.errorImageIsAssigned = true;
dxo.controlCellStyles = { cssClass: 'dxeErrorFrame_DevEx dxeErrorFrameSys dxeNoBorderRight dxeControlsCell_DevEx', style: '' };
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle('I','dxeInvalid_DevEx','');
dxo.styleDecoration.AddStyle('F','dxeFocused_DevEx','');
dxo.AfterCreate();

//-->

                            
		
			
		
	
&lt;!--
aspxAddDisabledItems('cpContent_pnlName_edit_user_ldap',[[['dxeDisabled_DevEx'],[''],['','I']]]);

var dxo = new ASPxClientTextBox('cpContent_pnlName_edit_user_ldap');
window['CL_edit_user_ldap'] = dxo;
dxo.uniqueID = 'ctl00$cpContent$pnlName$edit_user_ldap';
dxo.clientVisible = false;dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle('F','dxeFocused_DevEx','');
dxo.AfterCreate();

//-->

                        
                    
                    
                        Peran User *
                        
                        :
                        
                        
                            
		
			
				
					
				
			
				
					
						
							
								
									
										
											
												 
											
										
											
												Pilih salah satu
											
												Admin Kantor Pusat
											
												Admin KPw
											
												Konsultan
											
												Operator
											
										
									
								
							
&lt;!--
aspxAddDisabledItems('cpContent_pnlName_edit_user_roles_csv_DDD_L',[[['dxeDisabled_DevEx'],[''],['']]]);

var dxo = new ASPxClientListBox('cpContent_pnlName_edit_user_roles_csv_DDD_L');
window['cpContent_pnlName_edit_user_roles_csv_DDD_L'] = dxo;
dxo.uniqueID = 'ctl00$cpContent$pnlName$edit_user_roles_csv$DDD$L';
dxo.SelectedIndexChanged.AddHandler(function (s, e) { aspxCBLBSelectedIndexChanged('cpContent_pnlName_edit_user_roles_csv', e); });
dxo.ItemClick.AddHandler(function (s, e) { aspxCBLBItemMouseUp('cpContent_pnlName_edit_user_roles_csv', e); });
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle('F','dxeFocused_DevEx','');
dxo.savedSelectedIndex = 0;
dxo.itemsValue=['','Admin Kantor Pusat','Admin KPw','Konsultan','Operator'];
dxo.isComboBoxList = true;
dxo.hasSampleItem = true;
dxo.hoverClasses=['dxeListBoxItemHover_DevEx'];
dxo.selectedClasses=['dxeListBoxItemSelected_DevEx'];
dxo.disabledClasses=['dxeDisabled_DevEx'];
dxo.AfterCreate();

//-->

						
					
				
			
&lt;!--
aspxAddHoverItems('cpContent_pnlName_edit_user_roles_csv_DDD',[[['dxpc-closeBtnHover'],[''],['HCB-1']]]);

var dxo = new ASPxClientPopupControl('cpContent_pnlName_edit_user_roles_csv_DDD');
window['cpContent_pnlName_edit_user_roles_csv_DDD'] = dxo;
dxo.uniqueID = 'ctl00$cpContent$pnlName$edit_user_roles_csv$DDD';
dxo.Shown.AddHandler(function (s, e) { aspxDDBPCShown('cpContent_pnlName_edit_user_roles_csv', e); });
dxo.adjustInnerControlsSizeOnShow=false;
dxo.popupAnimationType='slide';
dxo.closeAction='CloseButton';
dxo.popupHorizontalAlign='LeftSides';
dxo.popupVerticalAlign='Below';
dxo.isPopupPositionCorrectionOn=false;
dxo.width=0;
dxo.height=0;
dxo.AfterCreate();

//-->

				
					Harus
                                        dipilih
				
			
		
	
		
			
				Loading…
			
		
	

	
&lt;!--
aspxAddHoverItems('cpContent_pnlName_edit_user_roles_csv',[[['dxeButtonEditButtonHover_DevEx'],[''],['B-1']]]);
aspxAddPressedItems('cpContent_pnlName_edit_user_roles_csv',[[['dxeButtonEditButtonPressed_DevEx'],[''],['B-1']]]);
aspxAddDisabledItems('cpContent_pnlName_edit_user_roles_csv',[[['dxeDisabled_DevEx'],[''],['','I']],[['dxeDisabled_DevEx dxeButtonDisabled_DevEx'],[''],['B-1'],,[[{'spriteCssClass':'dxEditors_edtDropDownDisabled_DevEx'}]],['Img']]]);
document.getElementById(&quot;cpContent_pnlName_edit_user_roles_csv_ET&quot;).setAttribute(&quot;errorFrame&quot;, &quot;errorFrame&quot;);
document.getElementById(&quot;cpContent_pnlName_edit_user_roles_csv_I&quot;).setAttribute(&quot;autocomplete&quot;, &quot;off&quot;);

var dxo = new ASPxClientComboBox('cpContent_pnlName_edit_user_roles_csv');
window['CL_Role'] = dxo;
dxo.callBack = function(arg) { WebForm_DoCallback('ctl00$cpContent$pnlName$edit_user_roles_csv',arg,aspxCallback,'cpContent_pnlName_edit_user_roles_csv',aspxCallbackError,true); };
dxo.uniqueID = 'ctl00$cpContent$pnlName$edit_user_roles_csv';
dxo.SelectedIndexChanged.AddHandler(function(s,e){ShowCbo();});
dxo.validationGroup = &quot;validate&quot;;
dxo.customValidationEnabled = true;
dxo.isValid = true;
dxo.errorText = 'Invalid value';
dxo.validationPatterns = [ new ASPxRequiredFieldValidationPattern('Harus\r\n                                        dipilih') ];
dxo.display = &quot;Dynamic&quot;;
dxo.errorImageIsAssigned = true;
dxo.controlCellStyles = { cssClass: 'dxeErrorFrame_DevEx dxeErrorFrameSys dxeNoBorderRight dxeControlsCell_DevEx', style: 'font-family:Helvetica;font-size:12px;' };
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle('I','dxeInvalid_DevEx','');
dxo.styleDecoration.AddStyle('F','dxeFocused_DevEx','');
dxo.incrementalFilteringMode='Contains';
dxo.lastSuccessValue = null;
dxo.islastSuccessValueInit = true;
dxo.AfterCreate();

//-->

                        
                    
                    
		KPw *
                        
		:
                        
		
                            
			
				
			
		
			
				
					
						
							
								
									
										
											Pilih salah satu
										
											KPw BALIKPAPAN
										
											KPw CIREBON
										
											KPw JEMBER
										
											KPw KEDIRI
										
											KPw LHOKSEUMAWE
										
											KPw MALANG
										
											KPw PEMATANGSIANTAR
										
											KPw PROVINSI ACEH
										
											KPw PROVINSI BALI
										
											KPw PROVINSI BANTEN
										
											KPw PROVINSI BENGKULU
										
											KPw PROVINSI D.I YOGYAKARTA
										
											KPw PROVINSI DKI JAKARTA
										
											KPw PROVINSI GORONTALO
										
											KPw PROVINSI JAMBI
										
											KPw PROVINSI JAWA BARAT
										
											KPw PROVINSI JAWA TENGAH
										
											KPw PROVINSI JAWA TIMUR
										
											KPw PROVINSI KALIMANTAN BARAT
										
											KPw PROVINSI KALIMANTAN SELATAN
										
											KPw PROVINSI KALIMANTAN TENGAH
										
											KPw PROVINSI KALIMANTAN TIMUR
										
											KPw PROVINSI KALIMANTAN UTARA
										
											KPw PROVINSI KEPULAUAN BANGKA BELITUNG
										
											KPw PROVINSI KEPULAUAN RIAU
										
											KPw PROVINSI LAMPUNG
										
											KPw PROVINSI MALUKU
										
											KPw PROVINSI MALUKU UTARA
										
											KPw PROVINSI NUSA TENGGARA BARAT
										
											KPw PROVINSI NUSA TENGGARA TIMUR
										
											KPw PROVINSI PAPUA
										
											KPw PROVINSI PAPUA BARAT
										
											KPw PROVINSI RIAU
										
											KPw PROVINSI SULAWESI BARAT
										
											KPw PROVINSI SULAWESI SELATAN
										
											KPw PROVINSI SULAWESI TENGAH
										
											KPw PROVINSI SULAWESI TENGGARA
										
											KPw PROVINSI SULAWESI UTARA
										
											KPw PROVINSI SUMATERA BARAT
										
											KPw PROVINSI SUMATERA SELATAN
										
											KPw PROVINSI SUMATERA UTARA
										
											KPw PURWOKERTO
										
											KPw SIBOLGA
										
											KPw SOLO
										
											KPw TASIKMALAYA
										
											KPw TEGAL
										
									
								
							
						
&lt;!--
aspxAddDisabledItems('cpContent_pnlName_edit_skdu_kpw_id_DDD_L',[[['dxeDisabled_DevEx'],[''],['']]]);

var dxo = new ASPxClientListBox('cpContent_pnlName_edit_skdu_kpw_id_DDD_L');
window['cpContent_pnlName_edit_skdu_kpw_id_DDD_L'] = dxo;
dxo.uniqueID = 'ctl00$cpContent$pnlName$edit_skdu_kpw_id$DDD$L';
dxo.SelectedIndexChanged.AddHandler(function (s, e) { aspxCBLBSelectedIndexChanged('cpContent_pnlName_edit_skdu_kpw_id', e); });
dxo.ItemClick.AddHandler(function (s, e) { aspxCBLBItemMouseUp('cpContent_pnlName_edit_skdu_kpw_id', e); });
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle('F','dxeFocused_DevEx','');
dxo.savedSelectedIndex = 0;
dxo.itemsValue=[null,2,8,13,14,17,18,24,3,9,26,7,32,44,10,11,5,35,29,34,6,22,39,46,42,40,4,36,43,37,16,12,45,41,47,20,38,15,33,21,23,19,25,27,28,30,31];
dxo.isComboBoxList = true;
dxo.hoverClasses=['dxeListBoxItemHover_DevEx'];
dxo.selectedClasses=['dxeListBoxItemSelected_DevEx'];
dxo.disabledClasses=['dxeDisabled_DevEx'];
dxo.AfterCreate();

//-->

					
				
			
		
&lt;!--
aspxAddHoverItems('cpContent_pnlName_edit_skdu_kpw_id_DDD',[[['dxpc-closeBtnHover'],[''],['HCB-1']]]);

var dxo = new ASPxClientPopupControl('cpContent_pnlName_edit_skdu_kpw_id_DDD');
window['cpContent_pnlName_edit_skdu_kpw_id_DDD'] = dxo;
dxo.uniqueID = 'ctl00$cpContent$pnlName$edit_skdu_kpw_id$DDD';
dxo.Shown.AddHandler(function (s, e) { aspxDDBPCShown('cpContent_pnlName_edit_skdu_kpw_id', e); });
dxo.adjustInnerControlsSizeOnShow=false;
dxo.popupAnimationType='slide';
dxo.closeAction='CloseButton';
dxo.popupHorizontalAlign='LeftSides';
dxo.popupVerticalAlign='Below';
dxo.isPopupPositionCorrectionOn=false;
dxo.width=0;
dxo.height=0;
dxo.AfterCreate();

//-->

&lt;!--
aspxAddHoverItems('cpContent_pnlName_edit_skdu_kpw_id',[[['dxeButtonEditButtonHover_DevEx'],[''],['B-1']]]);
aspxAddPressedItems('cpContent_pnlName_edit_skdu_kpw_id',[[['dxeButtonEditButtonPressed_DevEx'],[''],['B-1']]]);
document.getElementById(&quot;cpContent_pnlName_edit_skdu_kpw_id_I&quot;).setAttribute(&quot;autocomplete&quot;, &quot;off&quot;);

var dxo = new ASPxClientComboBox('cpContent_pnlName_edit_skdu_kpw_id');
window['cpContent_pnlName_edit_skdu_kpw_id'] = dxo;
dxo.uniqueID = 'ctl00$cpContent$pnlName$edit_skdu_kpw_id';
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle('F','dxeFocused_DevEx','');
dxo.incrementalFilteringMode='Contains';
dxo.lastSuccessValue = null;
dxo.islastSuccessValueInit = true;
dxo.AfterCreate();

//-->

                        
	
	
                    
                        Email *
                        
                        :
                        
                        
                            
		
			
				
					
				
			
				
					Invalid value
				
			
		
	
&lt;!--
aspxAddDisabledItems('cpContent_pnlName_edit_user_email',[[['dxeDisabled_DevEx'],[''],['','I']]]);
document.getElementById(&quot;cpContent_pnlName_edit_user_email_ET&quot;).setAttribute(&quot;errorFrame&quot;, &quot;errorFrame&quot;);

var dxo = new ASPxClientTextBox('cpContent_pnlName_edit_user_email');
window['CL_edit_user_email'] = dxo;
dxo.uniqueID = 'ctl00$cpContent$pnlName$edit_user_email';
dxo.validationGroup = &quot;validate&quot;;
dxo.customValidationEnabled = true;
dxo.isValid = true;
dxo.errorText = 'Invalid value';
dxo.validationPatterns = [ new ASPxRequiredFieldValidationPattern('Harus\r\n                                        diisi'), new ASPxRegularExpressionValidationPattern('Format e-mail salah', '\\w+([-+.\']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*') ];
dxo.setFocusOnError = true;
dxo.display = &quot;Dynamic&quot;;
dxo.errorImageIsAssigned = true;
dxo.controlCellStyles = { cssClass: 'dxeErrorFrame_DevEx dxeErrorFrameSys dxeNoBorderRight dxeControlsCell_DevEx', style: '' };
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle('I','dxeInvalid_DevEx','');
dxo.styleDecoration.AddStyle('F','dxeFocused_DevEx','');
dxo.AfterCreate();

//-->

                        
                    
                    
                        Alamat *
                        
                        :
                        
                        
                            
		
			
				
					
				
			
				
					Invalid value
				
			
		
	
&lt;!--
document.getElementById(&quot;cpContent_pnlName_edit_user_alamat_ET&quot;).setAttribute(&quot;errorFrame&quot;, &quot;errorFrame&quot;);

var dxo = new ASPxClientMemo('cpContent_pnlName_edit_user_alamat');
window['cpContent_pnlName_edit_user_alamat'] = dxo;
dxo.uniqueID = 'ctl00$cpContent$pnlName$edit_user_alamat';
dxo.validationGroup = &quot;validate&quot;;
dxo.customValidationEnabled = true;
dxo.isValid = true;
dxo.errorText = 'Invalid value';
dxo.validationPatterns = [ new ASPxRequiredFieldValidationPattern('Harus\r\n                                        diisi') ];
dxo.display = &quot;Dynamic&quot;;
dxo.errorImageIsAssigned = true;
dxo.controlCellStyles = { cssClass: 'dxeErrorFrame_DevEx dxeErrorFrameSys dxeNoBorderRight dxeControlsCell_DevEx', style: '' };
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle('I','dxeInvalid_DevEx','');
dxo.styleDecoration.AddStyle('F','dxeFocused_DevEx','');
dxo.AfterCreate();

//-->

                        
                    
                    
                        Telepon *
                        
                        :
                        
                        
                            
		
			
				
					
				
			
				
					Invalid value
				
			
		
	
&lt;!--
aspxAddDisabledItems('cpContent_pnlName_edit_user_telp',[[['dxeDisabled_DevEx'],[''],['','I']]]);
document.getElementById(&quot;cpContent_pnlName_edit_user_telp_ET&quot;).setAttribute(&quot;errorFrame&quot;, &quot;errorFrame&quot;);

var dxo = new ASPxClientTextBox('cpContent_pnlName_edit_user_telp');
window['cpContent_pnlName_edit_user_telp'] = dxo;
dxo.uniqueID = 'ctl00$cpContent$pnlName$edit_user_telp';
dxo.KeyPress.AddHandler(function(s,e){CekAngka(s,e);});
dxo.validationGroup = &quot;validate&quot;;
dxo.customValidationEnabled = true;
dxo.isValid = true;
dxo.errorText = 'Invalid value';
dxo.validationPatterns = [ new ASPxRequiredFieldValidationPattern('Harus\r\n                                        diisi') ];
dxo.display = &quot;Dynamic&quot;;
dxo.errorImageIsAssigned = true;
dxo.controlCellStyles = { cssClass: 'dxeErrorFrame_DevEx dxeErrorFrameSys dxeNoBorderRight dxeControlsCell_DevEx', style: '' };
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle('I','dxeInvalid_DevEx','');
dxo.styleDecoration.AddStyle('F','dxeFocused_DevEx','');
dxo.AfterCreate();

//-->

                        
                    
                    
                        Keterangan
                        
                        :
                        
                        
                            
		
			
		
	
&lt;!--

var dxo = new ASPxClientMemo('cpContent_pnlName_edit_user_description');
window['cpContent_pnlName_edit_user_description'] = dxo;
dxo.uniqueID = 'ctl00$cpContent$pnlName$edit_user_description';
dxo.RequireStyleDecoration();
dxo.styleDecoration.AddStyle('F','dxeFocused_DevEx','');
dxo.AfterCreate();

//-->

                        
                    
                
            

	
		
			Loading…
		
	



&lt;!--

var dxo = new ASPxClientCallbackPanel('cpContent_pnlName');
window['CL_pnlName'] = dxo;
dxo.callBack = function(arg) { WebForm_DoCallback('ctl00$cpContent$pnlName',arg,aspxCallback,'cpContent_pnlName',aspxCallbackError,true); };
dxo.uniqueID = 'ctl00$cpContent$pnlName';
dxo.EndCallback.AddHandler(OnEndCallbackRsp);
dxo.hideContentOnCallback=false;
dxo.AfterCreate();

//-->

    
    
        
            
                
                    
	
		Simpan
	

&lt;!--
aspxAddHoverItems('cpContent_btnSimpan',[[['dxbButtonHover_DevEx'],[''],[''],['','TC']]]);
aspxAddPressedItems('cpContent_btnSimpan',[[['dxbButtonPressed_DevEx'],[''],[''],['','TC']]]);
aspxAddDisabledItems('cpContent_btnSimpan',[[['dxbDisabled_DevEx'],[''],[''],['','TC']]]);

var dxo = new ASPxClientButton('cpContent_btnSimpan');
window['cpContent_btnSimpan'] = dxo;
dxo.uniqueID = 'ctl00$cpContent$btnSimpan';
dxo.Click.AddHandler(function(s, e) { if(ASPxClientEdit.AreEditorsValid()) { LoadingPanel.Show(); } });
dxo.RegisterServerEventAssigned(['Click']);
aspxAddSelectedItems('cpContent_btnSimpan',[[['dxbf'],[''],['CD']]]);
dxo.validationGroup = &quot;validate&quot;;
dxo.AfterCreate();

//-->

                
                
                    
	
		Reset Password
	

&lt;!--
aspxAddHoverItems('cpContent_btnReset',[[['dxbButtonHover_DevEx'],[''],[''],['','TC']]]);
aspxAddPressedItems('cpContent_btnReset',[[['dxbButtonPressed_DevEx'],[''],[''],['','TC']]]);
aspxAddDisabledItems('cpContent_btnReset',[[['dxbDisabled_DevEx'],[''],[''],['','TC']]]);

var dxo = new ASPxClientButton('cpContent_btnReset');
window['cpContent_btnReset'] = dxo;
dxo.uniqueID = 'ctl00$cpContent$btnReset';
dxo.clientVisible = false;dxo.Click.AddHandler(function(s, e) { ConfirmFirst(s,e); });
dxo.RegisterServerEventAssigned(['Click']);
aspxAddSelectedItems('cpContent_btnReset',[[['dxbf'],[''],['CD']]]);
dxo.AfterCreate();

//-->

                
            
        
    
    
	
		
			Loading…
		
	



&lt;!--

var dxo = new ASPxClientLoadingPanel('cpContent_LoadingPanel');
window['LoadingPanel'] = dxo;
dxo.uniqueID = 'ctl00$cpContent$LoadingPanel';
dxo.AfterCreate();

//-->


        

</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;form2&quot;)/div[3]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/SKDU V2/Page_User  BI Survei/iframe_Tambah User_popupMasterMain1_CIF-1</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form2']/div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]</value>
   </webElementXpaths>
</WebElementEntity>
