import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://10.239.5.15/survei/')

WebUI.setText(findTestObject('Object Repository/SKDU V2/Page_Bank Indonesia - Survei/input_Username_ctl00MainContentUserName'), 
    'datacaraka_admin')

WebUI.setEncryptedText(findTestObject('Object Repository/SKDU V2/Page_Bank Indonesia - Survei/input_Password_ctl00MainContentPassword'), 
    '7ykwm6oA3f6Xz5A4YZgZhA==')

WebUI.click(findTestObject('Object Repository/SKDU V2/Page_Bank Indonesia - Survei/input_Login sebagai pengguna internal Bank _0d2053'))

WebUI.click(findTestObject('Object Repository/SKDU V2/Page_Bank Indonesia - Survei/span_Survei Kegiatan Dunia Usaha Versi 20'))

WebUI.click(findTestObject('Object Repository/SKDU V2/Page_Home  BI Survei/span_Administrasi'))

WebUI.click(findTestObject('Object Repository/SKDU V2/Page_Home  BI Survei/div_Pengaturan'))

WebUI.click(findTestObject('Object Repository/SKDU V2/Page_Home  BI Survei/a_User'))

WebUI.click(findTestObject('Object Repository/SKDU V2/Page_User  BI Survei/div_Tambah'))

WebUI.setText(findTestObject('Object Repository/SKDU V2/Page_User  BI Survei/input__ctl00cpContentpnlNamenonLdapedit_user_id'), 
    'user_test01')

WebUI.setEncryptedText(findTestObject('Object Repository/SKDU V2/Page_User  BI Survei/input__ctl00cpContentpnlNameuser_secret_key'), 
    '7ykwm6oA3f6Xz5A4YZgZhA==')

WebUI.setText(findTestObject('Object Repository/SKDU V2/Page_User  BI Survei/input__ctl00cpContentpnlNameedit_user_nama'), 
    'user test')

WebUI.click(findTestObject('Object Repository/SKDU V2/Page_User  BI Survei/img__cpContent_pnlName_edit_user_roles_csv_B-1Img'))

WebUI.click(findTestObject('Object Repository/SKDU V2/Page_User  BI Survei/td_Operator'))

WebUI.click(findTestObject('Object Repository/SKDU V2/Page_User  BI Survei/img__cpContent_pnlName_edit_skdu_kpw_id_B-1Img'))

WebUI.click(findTestObject('Object Repository/SKDU V2/Page_User  BI Survei/td_KPw PROVINSI BENGKULU'))

WebUI.setText(findTestObject('Object Repository/SKDU V2/Page_User  BI Survei/input__ctl00cpContentpnlNameedit_user_email'), 
    'a@b.c')

WebUI.setText(findTestObject('Object Repository/SKDU V2/Page_User  BI Survei/textarea__ctl00cpContentpnlNameedit_user_alamat'), 
    'abc')

WebUI.setText(findTestObject('Object Repository/SKDU V2/Page_User  BI Survei/input__ctl00cpContentpnlNameedit_user_telp'), 
    '123')

WebUI.setEncryptedText(findTestObject('Object Repository/SKDU V2/Page_User  BI Survei/input__ctl00cpContentpnlNameuser_secret_key'), 
    '7ykwm6oA3f6Xz5A4YZgZhA==')

WebUI.click(findTestObject('Object Repository/SKDU V2/Page_User  BI Survei/div_Simpan'))

