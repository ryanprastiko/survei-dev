import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://10.239.5.15/survei/')

WebUI.setText(findTestObject('Page_Bank Indonesia - Survei/input_Username_ctl00MainContentUserName'), 'datacaraka_admin')

WebUI.setEncryptedText(findTestObject('Page_Bank Indonesia - Survei/input_Password_ctl00MainContentPassword'), '7ykwm6oA3f6Xz5A4YZgZhA==')

WebUI.click(findTestObject('Page_Bank Indonesia - Survei/input_Login sebagai pengguna internal Bank _0d2053'))

WebUI.click(findTestObject('Page_Bank Indonesia - Survei/span_Survei Kegiatan Dunia Usaha Versi 20'))

WebUI.click(findTestObject('Object Repository/SKDU V2/Page_Home  BI Survei/div_Administrasi'))

WebUI.click(findTestObject('Object Repository/SKDU V2/Page_Home  BI Survei/div_Absensi'))

WebUI.click(findTestObject('Object Repository/SKDU V2/Page_Home  BI Survei/li_Absensi SKDU'))

WebUI.click(findTestObject('Object Repository/SKDU V2/Page_Absensi SKDU  BI Survei/img__cpContent_cboTahun_B-1Img'))

WebUI.click(findTestObject('Object Repository/SKDU V2/Page_Absensi SKDU  BI Survei/td_2019'))

WebUI.click(findTestObject('Object Repository/SKDU V2/Page_Absensi SKDU  BI Survei/img__cpContent_cboTriwulan_B-1Img'))

WebUI.click(findTestObject('Object Repository/SKDU V2/Page_Absensi SKDU  BI Survei/td_4'))

WebUI.click(findTestObject('Object Repository/SKDU V2/Page_Absensi SKDU  BI Survei/div_Cari'))

WebUI.click(findTestObject('Object Repository/SKDU V2/Page_Absensi SKDU  BI Survei/div_Export ke Excel'))

